# create databases
CREATE DATABASE IF NOT EXISTS `wordpress_db`;
CREATE DATABASE IF NOT EXISTS `bitrix_db`;
CREATE DATABASE IF NOT EXISTS `joomla_db`;

# create root user and grant rights
CREATE USER 'db-user'@'localhost' IDENTIFIED BY '9UqfqeE9#hn2@t^';
GRANT ALL ON *.* TO 'db-user'@'%';